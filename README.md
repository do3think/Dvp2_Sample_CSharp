# Dvp2_Sample_CSharp

### 介绍
这是度申相机C#的Demo

### 注意事项：
每个项目的.csproj里面我都根据不同平台配置了不同位数dll库
```
    <Reference Condition=" '$(Platform)' == 'x86'" Include="DVPCameraCS, Version=1.0.3.0, Culture=neutral, processorArchitecture=x86">
      <SpecificVersion>False</SpecificVersion>
      <HintPath>..\lib\x86\DVPCameraCS.dll</HintPath>
    </Reference>
	<Reference Condition=" '$(Platform)' == 'x64'" Include="DVPCameraCS, Version=1.0.3.0, Culture=neutral, processorArchitecture=x64">
      <SpecificVersion>False</SpecificVersion>
      <HintPath>..\lib\x64\DVPCameraCS64.dll</HintPath>
    </Reference> 
```
### 更新记录： 
2020/09/08 cyn  
1、修复多相机设置User ID同步更新问题  
2、对不同编译平台添加配置文件，这样不需要手动选择引用库目录  


#### 第一个案例BasicFunction

![](./Image/BasicFunction.png)

#### 第二个案例ImageAcquisition

![](./Image/ImageAcquisition.png)

#### 第三个案例MultipleCamera

![](./Image/MultipleCamera.png)

#### 第四个案例Trigger

![](./Image/Trigger.png)

如有问题，可提交issue，我会尽快解答。
